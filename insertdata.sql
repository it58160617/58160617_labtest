INSERT INTO Department VALUES ('D001','Accounting');
INSERT INTO Department VALUES ('D002','Information Technology');
INSERT INTO Department VALUES ('D003','Marketing');

Insert into Manager values ('T001','Aunchasa','Rukdee','D001');
Insert into Manager values ('T002','Sarayut','Pattanun','D002');
Insert into Manager values ('T003','Panuwat','Sakdaporn','D003');

insert into Employee values ('58161111','Petchara','Bunlerd','D001','T001');
insert into Employee values ('58162222','Jerapat','Pansrida','D002','T002');
insert into Employee values ('58163333','Suchada','Buasri','D003','T003');

