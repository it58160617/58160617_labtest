create view ManageEmployee as
select e.EmployeeID, e.EmployeeName, e.EmployeeSurname,
	d.DeptName, m.ManagerName, m.ManagerSurname
from Employee as e
LEFT JOIN ( Department as d, Manager as m)
ON ( e.DeptID = d.DeptID and e.ManagerID = m.ManagerID);
