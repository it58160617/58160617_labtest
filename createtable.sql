drop table if exists Employee;
drop table if exists Manager;
drop table if exists Department;

create table Department
(
        DeptID          varchar(10)     not null,
        DeptName        varchar(30)     not null,
        PRIMARY KEY(DeptID)
) Engine=InnoDB;

create table Manager
(
        ManagerID       varchar(10)     not null,
        ManagerName     varchar(30)     not null,
        ManagerSurname  varchar(30)     not null,
        DeptID          varchar(10)     not null,
        PRIMARY KEY(ManagerID),
        FOREIGN KEY(DeptID) REFERENCEs Department(DeptID)
) Engine=InnoDB;

create table Employee
(
        EmployeeID      varchar(10)     not null,
        EmployeeName    varchar(30)     not null,
        EmployeeSurname varchar(30)     not null,
        DeptID          varchar(10)     not null,
        ManagerID       varchar(10)     not null,
        PRIMARY KEY(EmployeeID),
        FOREIGN KEY(DeptID) REFERENCES Department(DeptID),
        FOREIGN KEY(ManagerID) REFERENCES Manager(ManagerID)
) Engine=InnoDB;
